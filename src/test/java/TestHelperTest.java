import org.junit.jupiter.api.Test;

import javax.swing.text.StyledEditorKit;
import javax.xml.crypto.Data;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class TestHelperTest {

    @Test
    void randomNumberTest() {
        assertEquals(true,checkRandomNumber(TestHelper.randomNumber()));
    }

    @Test
    void randomLongTest() {

        assertEquals(true, checkRandomLong(TestHelper.randomLong()));
    }

    @Test
    void randomStringTest_3wordInTheString() {
        //Jdhbls Jljndlsjkn LKJhfldjn
        assertEquals(true, chech3wordInTheString(TestHelper.randomString()));
    }

    @Test
    void readHashMapTest() {
        try {
            assertEquals(true, checkHashMap(TestHelper.readHashMap()));
        }
        catch (FileNotFoundException e)
        {

        }

    }



    @Test
    void dateParser() {
        Date date  = new Date();
        assertNotEquals(date.toString(), TestHelper.dateParser(date));
    }

    @Test
    void stringParser() {
        assertEquals(true, checkDouble(TestHelper.stringParser("0.0")));
        assertEquals(true, checkDouble(TestHelper.stringParser("dss")));
    }

    private boolean checkRandomNumber (Integer randomInt)
    {
        if (randomInt>1970 && randomInt<2021)
             return true;
        else
            return false;
    }

    private  boolean checkRandomLong (Object obj)
    {
        try {
            long randomLong = 0;
            randomLong = (long)obj;
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    private boolean chech3wordInTheString (String str)
    {
     try {
         if((str.length() - str.replaceAll(" ", "").length()) == 2)
         return true;
         else
             return false;
     }
     catch (Exception e)
     {
         return false;
     }
    }

    private boolean checkDouble (Object obj)
    {
        try
        {
            Double dbl = (Double) obj;
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private boolean checkHashMap(HashMap readHashMap) {
        try {
            if((int)readHashMap.get("first")==1 &&
                    (int)readHashMap.get("second")==2 &&
                    (int)readHashMap.get("third")==3 &&
                    (int)readHashMap.get("fourth")==4)
            {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
    }
}