import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestHelper {

    //  1.	выдает случайное число от 1970 до текущего года
    public static int randomNumber ()
    {
        return getNumber();
    }

    //	2.	выдает случайное число типа long
    public static long randomLong ()
    {
        return getLong();
    }

    //	3.	генерирует случайную строку (три слова c больших букв разумной длины через пробел, например - « Ghgfdf Ndryd Poggdq »)
    public static String  randomString ()
    {
        return getString() + " " + getString() + " " + getString();
    }

    //	4.	прочитает файл из нескольких строк (каждая в формате « Ключ :: Значение » ) и составит из них HashMap.
    public static HashMap readHashMap () throws FileNotFoundException {
        return getHashMap();
    }

    //	5.	преобразует один формат даты в другой
    public static String dateParser (Date date)
    {
        return getParseDate(date);
    }

    //	6.	превращает строку в число Double и, если это невозможно, возвращает Infinity
    public static double stringParser (String stringDouble)
    {
        return getParseString(stringDouble);
    }

    //  1.	выдает случайное число от 1970 до текущего года
    private static int getNumber()
    {
        int min = 1970;
        int max = 2021;
        int diff = max - min;
        Random random = new Random();
        return random.nextInt(diff + 1) + min;
    }

    //	2.	выдает случайное число типа long
    private static long getLong ()
    {
        Random random = new Random();
        return random.nextLong();
    }

    //	3.	генерирует случайную строку (три слова c больших букв разумной длины через пробел, например - « Ghgfdf Ndryd Poggdq »)
    private static String  getString ()
    {
        String str = "abcdefghijklmnopqrstuvwxyz";
        String firstLetter = str.charAt((int)(Math.random()*str.length())) + "";
        String outString = "";
        for (int i = 0; i<23; i++)
        {
            outString += str.charAt((int)(Math.random()*str.length()));
        }
        return firstLetter.toUpperCase(Locale.ROOT) + outString;
    }

    //	4.	прочитает файл из нескольких строк (каждая в формате « Ключ :: Значение » ) и составит из них HashMap.
    private static HashMap getHashMap () {
        HashMap <String,Object> hashMap = new HashMap<>();
        try {
            String jsonString = readFileAsString ("src/main/resources/customerID.json");
            JSONObject jsonObject = new JSONObject(jsonString);
            Map<String, Object> jsonMap =  jsonObject.toMap();
            hashMap.putAll(jsonMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    private static String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }


    //	5.	преобразует один формат даты в другой
    private static String getParseDate (Date date)
    {
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        return newDateFormat.format(date);
    }

    //	6.	превращает строку в число Double и, если это невозможно, возвращает Infinity
    private static double getParseString (String stringDouble) {
        try
        {
            return Double.parseDouble(stringDouble);
        }catch (Exception e)
        {
            return Double.POSITIVE_INFINITY;
        }
    }

}
