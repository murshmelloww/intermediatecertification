import java.io.FileNotFoundException;
import java.util.Date;

public class IntermediateCertification {

    public static void main (String[] args) throws FileNotFoundException {

        System.out.println(TestHelper.dateParser(new Date()));
        System.out.println(TestHelper.randomLong());
        System.out.println(TestHelper.randomNumber());
        System.out.println(TestHelper.randomString());
        System.out.println(TestHelper.stringParser("0"));
        System.out.println(TestHelper.readHashMap());
    }
}
